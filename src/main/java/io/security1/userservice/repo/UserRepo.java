package io.security1.userservice.repo;

import io.security1.userservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author beom
 * @version 1.0
 * @since 20/09/2021
 */
public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
