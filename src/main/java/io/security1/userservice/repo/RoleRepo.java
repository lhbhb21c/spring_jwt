package io.security1.userservice.repo;

import io.security1.userservice.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author beom
 * @version 1.0
 * @since 20/09/2021
 */
public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
