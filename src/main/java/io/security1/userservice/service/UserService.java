package io.security1.userservice.service;

import io.security1.userservice.domain.Role;
import io.security1.userservice.domain.User;

import java.util.List;

/**
 * @author beom
 * @version 1.0
 * @since 20/09/2021
 */
public interface UserService {
    User saveUser(User user);

    Role saveRole(Role role);

    void addRoleToUser(String username, String roleName);

    User getUser(String username);

    List<User> getUsers();
}
